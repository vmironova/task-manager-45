package ru.t1consulting.vmironova.tm.api.service.dto;

import ru.t1consulting.vmironova.tm.dto.model.SessionDTO;

public interface ISessionDTOService extends IUserOwnedDTOService<SessionDTO> {
}
