package ru.t1consulting.vmironova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.vmironova.tm.dto.model.ProjectDTO;

public interface IProjectDTORepository extends IUserOwnedDTORepository<ProjectDTO> {

    @NotNull
    ProjectDTO create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description
    ) throws Exception;

    @NotNull
    ProjectDTO create(
            @NotNull String userId,
            @NotNull String name
    ) throws Exception;

}
