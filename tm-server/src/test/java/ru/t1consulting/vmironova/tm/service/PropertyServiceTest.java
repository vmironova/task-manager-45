package ru.t1consulting.vmironova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1consulting.vmironova.tm.api.service.IPropertyService;
import ru.t1consulting.vmironova.tm.marker.UnitCategory;

@Category(UnitCategory.class)
public final class PropertyServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Test
    @Ignore
    public void getApplicationVersion() {
        Assert.assertNotNull(propertyService.getApplicationVersion());
    }

    @Test
    @Ignore
    public void getAuthorName() {
        Assert.assertNotNull(propertyService.getAuthorName());
    }

    @Test
    @Ignore
    public void getAuthorEmail() {
        Assert.assertNotNull(propertyService.getAuthorEmail());
    }

    @Test
    public void getServerPort() {
        Assert.assertNotNull(propertyService.getServerPort());
    }

    @Test
    public void getServerHost() {
        Assert.assertNotNull(propertyService.getServerHost());
    }

    @Test
    public void getSessionKey() {
        Assert.assertNotNull(propertyService.getSessionKey());
    }

    @Test
    public void getSessionTimeout() {
        Assert.assertNotNull(propertyService.getSessionTimeout());
    }

    @Test
    public void getDBUser() {
        Assert.assertNotNull(propertyService.getDBUser());
    }

    @Test
    public void getDBPassword() {
        Assert.assertNotNull(propertyService.getDBPassword());
    }

    @Test
    public void getDBUrl() {
        Assert.assertNotNull(propertyService.getDBUrl());
    }

    @Test
    public void getDBDriver() {
        Assert.assertNotNull(propertyService.getDBDriver());
    }

}
